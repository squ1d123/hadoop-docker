#!/bin/bash

echo Configuring livy

for c in `printenv | perl -sne 'print "$1 " if m/^LIVY_CONF_(.+?)=.*/'`; do
    name=`echo ${c} | perl -pe 's/___/-/g; s/__/_/g; s/_/./g'`
    var="LIVY_CONF_${c}"
    value=${!var}
    echo "Setting LIVY property $name=$value"
    echo $name = $value >> $LIVY_CONF_DIR/livy.conf
done

exec /spark-entrypoint.sh $@
